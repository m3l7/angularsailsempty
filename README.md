# Angular Sails generator

A minimal generator for sails with angular, angular-data, angular-moment, security, logging, sass support.

## Install

clone the repo and install dependencies:

      npm install
      bower install

## Start the server

      sails lift

or

      node app.js

## Production

Build the assets folder into .tmp/public:

      grunt buildtmp

start the server in production mode (it's just an empty grunt task):

      sails lift --prod

or

      node app.js --prod

WARNING: Note that the build process will overwrite the same .tmp/public possibily causing downtimes 

## Backendless development ($httpbackend)

set MOCK=true to enable a fake mocked backend, i.e.:

      MOCK=true sails lift
      MOCK=true node app.js
      MOCK=true grunt buildtmp
