module.exports = function(grunt) {

  grunt.config.set('filerev', {
      src: [
        '.tmp/public/scripts/{,*/}*.js',
        '.tmp/public/styles/{,*/}*.css',
        '.tmp/public/content/images/{,*/}*.{png,jpg,jpeg,gif,webp,svg}',
        '.tmp/public/content/fonts/*'
      ]
  });

  grunt.loadNpmTasks('grunt-filerev');
};
