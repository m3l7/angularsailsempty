/**
* Users.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
    email: "string",
    username: {
      type: "string",
      unique: true
    },
    password: "string",
    role: "string",
    token: "string",
    sanitize: function(){
      delete this.password;
      delete this.token;
      return this;
    }
  },
  beforeCreate: function (attrs, next) {

    var bcrypt = require('bcrypt');

    bcrypt.genSalt(10, function(err, salt) {
      if (err) next(err);

      bcrypt.hash(attrs.password, salt, function(err, hash) {
        if (err) next(err);

        attrs.password = hash;
        next();
      });
    });
  },
  beforeUpdate: function(attrs,next){

    if (!!attrs.password){

        var bcrypt = require('bcrypt');

        bcrypt.genSalt(10, function(err, salt) {

          if (err) next(err);

          bcrypt.hash(attrs.password, salt, function(err, hash) {

            if (err) next(err);

            attrs.password = hash;
            next();
          });
        });

    }
    else next();
  }
};