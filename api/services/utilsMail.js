// var nodemailer = require('nodemailer');

var utilsMail = {
    sendPasswordMail: function(email,user,password){

        // create reusable transport method (opens pool of SMTP connections)
        var smtpTransport = nodemailer.createTransport("SMTP",{
            service: sails.config.mail.service,
            auth: {
                user: sails.config.mail.user,
                pass: sails.config.mail.pass
            }
        });

        // setup e-mail data with unicode symbols
        var emailText = "<p>I tuoi dati di accesso al Caseificio San Rocco sono: </p><p>User: "+user+"</p><p>Pass: "+password+"</p><p><a href='http://www.caseificiosanrocco.it'>caseificiosanrocco.it</a><p>";

        var mailOptions = {
            from: "Caseificio San Rocco ✔ <"+sails.config.mail.email+"+>", // sender address
            to: email, // list of receivers
            subject: "Dati di accesso al Caseificio San Rocco", // Subject line
            html: emailText
        }

        // send mail with defined transport object
        smtpTransport.sendMail(mailOptions, function(error, response){
            if(error){
                console.log(error);
            }else{
                console.log("Message sent: " + response.message);
            }
        });
    }
};

// module.exports = UtilsMail;