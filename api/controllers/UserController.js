/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */
var passport = require('passport');
module.exports = {
    passport_local: [
        passport.authenticate('local'),
        function(req,res){
            res.send(req._passport.session.user);
        }
    ],

    logout: function(req,res){
        sails.models.user.findOne({id:req.user.id})
        .then(function(user){
            if (!user) res.serverError('Invalid User');
            else{
                sails.models.user.update({id:user.id},{token: ''})
                .then(function(){
                    res.send();                    
                });
            }
        })
        .catch(function(err){
            res.serverError(err);
        })
    },
    create: function(req,res){
        var body = req.body;
        // debugger
        if ((!body) || (!body.username) || (!body.email) || (!body.role)) res.badRequest('nome utente, password, email o role mancanti');
        else{
            //generate password
            if (!body.password) body.password = utilsCrypt.simplePassword(5);
            var password = body.password;

            sails.models.user.create(body).then(function(user){
                delete user.password;
                // utilsMail.sendPasswordMail(user.email,user.username,user.password);
                res.send(user);
            })
            .catch(function(err){
                res.serverError(err);
            });
        }
    },
    update: function(req,res){
        var id = req.params.id;
        var body = req.body;
        var reset = false;
        if ((!!body) && (body.reset)) {
            reset = true;
            delete body.reset;
            body.password = utilsCrypt.simplePassword(5);
        }
        var password = body.password;
        if (!id) res.badRequest();
        else sails.models.user.update({id:id},body).then(function(user){
            if (!user.length) res.badRequest();
            else if (reset){
                //reset password
                user[0].password = password;
                utilsMail.sendPasswordMail(user[0].email,user[0].username,user[0].password);
                res.send(user[0]);
            }
            else res.send(user[0]);
        }) 
        .catch(function(err){
            res.serverError(err);
        })
    },
    findOne: function(req,res){
        if (req.user.id!=req.params.id) res.forbidden();
        else{
            res.send(req.user);
        }
    }
};