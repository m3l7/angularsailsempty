(function(){
  angular
    .module('app')
    .config(Routes);

    Routes.$inject('$routeProvider');

    function Routes($routeProvider) {
      $routeProvider
        .when('/', {
          templateUrl: 'pages/home.html',
          controller: 'Home',
        });
    };  
})();
