(function(){
    angular
        .module('app')
        .run(dataConfig)

        dataConfig.$inject = ['DS','config'];

        function dataConfig(DS,config){
            angular.extend(DS.defaults,config.API);
        }
})();