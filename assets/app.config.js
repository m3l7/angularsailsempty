(function(){
    var config = {
        logging:{
          http: true
        },
        API:{
          baseUrl: '/apiv1/',
          baseUrlAssets: 'http://localhost:1337/',
          idAttribute: 'id',
        },
        lang:{
          langs: ['it','en','de','fr'],
          defaultLang: 'it'
        },
        url:{
          prefix: '/#!'
        }
    }

    angular
        .module('app')
        .constant('config',config)
        .config(['$locationProvider', function($location) {
          $location.hashPrefix('!');
        }]);
})();
