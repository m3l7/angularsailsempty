(function(){
    angular
        .module('app')
        .config(italianLanguage);

        italianLanguage.$inject = ['$translateProvider'];

        function italianLanguage($translateProvider){

            //set the path for languages
            $translateProvider.useStaticFilesLoader({
              prefix: '/languages/',
              suffix: '.json'
            });

            $translateProvider.translations('it', {

                //CONTATTI
                    CHI_SIAMO: 'Chi Siamo',
                    CONTATTACI: 'Contattaci',


            })            

            $translateProvider.preferredLanguage('it');
        }
})();