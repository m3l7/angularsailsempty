var passport = require('passport')
    , LocalStrategy = require('passport-local').Strategy
    , BearerStrategy = require('passport-http-bearer').Strategy
    , bcrypt = require('bcrypt')


passport.serializeUser(function(user, done) {
    done(null, user);
});

passport.deserializeUser(function(id, done) {
    sails.models.user.findOneById(id).done(function (err, user) {
        done(err, user);
    });
});

passport.use(new LocalStrategy({
        usernameField: 'username',
        passwordField: 'password'
    },
    function(username, password, done) {
        if ((!username) || (!password)) return done(null, false, { message: 'Utente o Password sbagliati.'});

        sails.models.user.findOne({ username: username})
        .then(function(user) {
            if (!user) { return done(null, false, { message: 'Utente o Password sbagliati.'}); }
            else{
                bcrypt.compare(password, user.password, function(err, resp) {
                    if (!!err) return done(err)
                    else if (!resp) return done(null,false,{message: 'Utente o Password sbagliati.'})
                    else {

                        //generate and save auth token
                        var token = utilsCrypt.uid(16);
                        user.token = token;
                        sails.models.user.update({id: user.id},{token: token})
                        .then(function(id){
                            return done(null, {
                                id: user.id,
                                email: user.email,
                                role: user.role,
                                token: token,
                                username: user.username
                            });
                        })
                        .catch(function(err){
                            return done(err);
                        })
                    }
                })
            }
        })
        .catch(function(err){
            return done(err);
        });
    }
));

passport.use(new BearerStrategy(
  function(accessToken, done) {
    sails.models.user.findOne({'token':accessToken})
    .then(function(user) {
        if (!user) { return done(null, false); }
        else return done(null,{
            id: user.id,
            email: user.email,
            role: user.role,
            username: user.username
        })
    })
    .catch(function(err){
        return done(err);
    })
  }
));